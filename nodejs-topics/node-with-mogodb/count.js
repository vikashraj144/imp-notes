var MongoClient = require('mongodb').MongoClient,
  test = require('assert');
MongoClient.connect('mongodb://vikash:welcome@ds111648.mlab.com:11648/vikashraj144test', function(err, db) {
  // Crete the collection for the distinct example
  var collection = db.collection('ServerPort');
  // Insert documents to perform distinct against
  collection.insertMany([{a:1}, {a:2}
    , {a:3}, {a:4, b:1}], {w: 1}, function(err, ids) {

    // Perform a total count command
    collection.count(function(err, count) {
      test.equal(null, err);
      test.equal(4, count);

      // Peform a partial account where b=1
      collection.count({b:1}, function(err, count) {
        test.equal(null, err);
        test.equal(1, count);

        db.close();
      });
    });
  });
});