# Basic Types
## Boolean
The most basic datatype is the simple true/false value
let isDone: boolean = false;

## Number
let decimal: number = 6;

## String
uses double quotes (") or single quotes (') to surround string data.
let color: string = "blue";
color = 'red';
You can also use template strings, which can span multiple lines  backtick/backquote (`)

let fullName: string = `Bob Bobbington`;
let age: number = 37;
let sentence: string = `Hello, my name is ${ fullName }.

I'll be ${ age + 1 } years old next month.`;

## Array
let list: number[] = [1, 2, 3];
    or
let list: Array<number> = [1, 2, 3];

## Tuple
epresent a value as a pair of a string and a number:
// Declare a tuple type
let x: [string, number];
// Initialize it
x = ["hello", 10]; // OK

## Enum
A helpful addition to the standard set of datatypes from JavaScript is the enum. 
enum Color {Red, Green, Blue}
let c: Color = Color.Green;

## Any

## Void

## Null and Undefined
let u: undefined = undefined;
let n: null = null;
