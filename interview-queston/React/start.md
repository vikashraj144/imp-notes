## for react js

### npm install -g create-react-app

### create-react-app my-app

### If you have npm 5.2.0+ installed, you may use npx instead.
npx create-react-app my-app
  
### npm start
    Starts the development server.

### npm run build
    Bundles the app into static files for production.

### npm test
    Starts the test runner.

### npm run eject
    Removes this tool and copies build dependencies, configuration files
    and scripts into the app directory. If you do this, you can’t go back!

### We suggest that you begin by typing:

  cd my-app2
  npm start